setInterval(function() {
  if ($("#transcription").val().indexOf("clear text") > -1) {
    $("#clear-all").click();
    $("button.add-to-cart").removeClass("added");
    $('button.add-to-cart').text('Add to Cart');
  }
  if ($("#transcription").val().indexOf("change theme") > -1) {
    $("#changeTheme").click();
    $("#clear-all").click();
  }
  if ($("#transcription").val().indexOf("add to cart") > -1) {
    $("button.add-to-cart").addClass("added");
    $('button.add-to-cart').text('Added to Cart');
    $("#clear-all").click();
  }
}, 500);

$('#changeTheme').click(function() {
  $('.mobile-wrap').toggleClass('light');
});

// Test browser support
window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || null;
if (window.SpeechRecognition === null) {
  document.getElementById('ws-unsupported').classList.remove('hidden');
  document.getElementById('button-play-ws').setAttribute('disabled', 'disabled');
  document.getElementById('button-stop-ws').setAttribute('disabled', 'disabled');
} else {
  var recognizer = new window.SpeechRecognition();
  var transcription = document.getElementById('transcription');
  var log = document.getElementById('log');
  // Recogniser doesn't stop listening even if the user pauses
  recognizer.continuous = true;
  // Start recognising
  recognizer.onresult = function(event) {
    transcription.textContent = '';
    for (var i = event.resultIndex; i < event.results.length; i++) {
      if (event.results[i].isFinal) {
        // show confidence ==> transcription.textContent = event.results[i][0].transcript + ' (Confidence: ' + event.results[i][0].confidence + ')';
        transcription.textContent = event.results[i][0].transcript;
      } else {
        transcription.textContent += event.results[i][0].transcript;
      }
    }
  };
  // Listen for errors
  recognizer.onerror = function(event) {
    log.innerHTML = 'Recognition error: ' + event.message + '<br />' + log.innerHTML;
  };
  document.getElementById('button-play-ws').addEventListener('click', function() {
    // Set if we need interim results
    recognizer.interimResults = document.querySelector('input[name="recognition-type"][value="interim"]').checked;
    try {
      recognizer.start();
      log.innerHTML = 'Recognition started' + '<br />' + log.innerHTML;
    } catch (ex) {
      log.innerHTML = 'Recognition error: ' + ex.message + '<br />' + log.innerHTML;
    }
  });
  document.getElementById('button-stop-ws').addEventListener('click', function() {
    recognizer.stop();
    log.innerHTML = 'Recognition stopped' + '<br />' + log.innerHTML;
  });
  document.getElementById('clear-all').addEventListener('click', function() {
    transcription.textContent = '';
    log.textContent = '';
  });
}